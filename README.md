## What is purpose?
Install Crowd using Vagrant

## Environment
- Provider : KVM(tested ubuntu 19.04), Virtualbox(tested windows 10, ubuntu 19.04)
- Provisioner : Ansible
- Box : Centos/7

## Keep to know
- If you use vagrant on Windows, you should install vagrant-guest_ansible plugin
  - https://github.com/vovimayhem/vagrant-guest_ansible
  run the command before you run the "vagrant up"
  vagrant plugin install vagrant-guest_ansible; vagrant up

## How to use
Run "vagrant up" in the directory which is with Vagrantfile
After done, You can see the Crowd first setup page at the localhost:8080.

If you wanna own database setup, you can setup with MySQL DB.
This playbook already has mysql-community-server(5.7).

You can set Database through below DB information. (JDBC)
- DB host: 127.0.0.1
- DB name: crowd
- DB user: crowduser
- DB password: crowduser

## Final stage
You can access http://localhost:8035 and you can find the Crowd initial page.
There is no license file. so you should register trial license in your atlassian homepage.
So, you should register your Crowd and get the trial license.